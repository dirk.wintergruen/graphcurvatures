import pyximport
from network_extensions.igraphx import multilayer
#pyximport.install(pyimport=True)

import sys
import os
import igraph
from FormanRicci import  __file__ as f
#print(f)
import FormanRicci.FormanRicci
import FormanRicci.InfluenceNetwork

#print(FormanRicci.__dict__)
#from igraphx import multilayer

#from FormanRicci.InfluenceNetwork import *
#from FormanRicci.FormanRicci import *

import time

print (os.getcwd())
class Timer():

    def __init__(self):
        self.timer = time.time()
        self.last_time = self.timer

    def usedTime(self,txt):
        end = time.time()
        step_time = end - self.last_time
        all_time = end - self.timer

        print("%s: %s %s" % (txt,step_time,all_time))
        self.last_time = time.time()


timer = Timer()





print("load ML")
ml = multilayer.MultiLayerGraph.load("data/multilayer_co_author_influence_collaboration.pickle")
timer.usedTime("ML")
gr  =ml.ynws[1920].multi_layer

gamma_gr = FormanRicci.InfluenceNetwork.gamma(gr)
timer.usedTime("gamma")
omega_gr = FormanRicci.InfluenceNetwork.omega(gr,gamma_gr)
timer.usedTime("omega")

nw = igraph.Graph.Weighted_Adjacency(gamma_gr.tolist(),mode = igraph.ADJ_MAX)

nw.vs["weight"] = omega_gr
timer.usedTime("graph")
nw.save("/tmp/nw.graphml")
FR = FormanRicci.FormanRicci.FormanRicchi()
FR.init_F2(nw,use_dict=False,use_dict2=False)

fn = FR.forman_edges_df(pool=10)
timer.usedTime("forman_edges")
with open("/tmp/fn3.out","w") as out:
        for f in fn:
            out.write("%s\n" % f)
timer.usedTime("saved fn3")

nw.es["forman"] = fn

fn_v  = FR.forman_vertices_df(pool=10,debug=True)
timer.usedTime("forman_vertices")
nw.vs["forman"] = fn_v

fo  = FR.forman_edges(pool=10)
timer.usedTime("old forman_edges")
nw.es["forman_old"] =fo

with open("/tmp/fo.out","w") as out:
    for f in fo:
        out.write("%s\n" % f)

        
#print(fo)
print("------")
#print(fn)

assert fo == fn

fo_v = FR.forman_vertices(pool=10,debug=True)
timer.usedTime("old forman_vertices")
nw.vs["forman_old"] =fo
assert fn_v == fo_v

nw.save("data/out.graphml")
