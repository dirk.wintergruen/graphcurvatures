import pyximport
from network_extensions.igraphx import multilayer
pyximport.install(pyimport=True)
import cProfile, pstats, io
from pstats import SortKey
pr = cProfile.Profile()
import sys
import os
import igraph
from FormanRicci import  __file__ as f
print("F",f)
import FormanRicci.FormanRicci
print("FF:", FormanRicci.FormanRicci.__file__)
#import FormanRicci.InfluenceNetwork
#import FormanRicci
#print(FormanRicci.__dict__)
#from igraphx import multilayer

#from FormanRicci.InfluenceNetwork import *
#from FormanRicci.FormanRicci import *

import time

print (os.getcwd())
class Timer():

    def __init__(self):
        self.timer = time.time()
        self.last_time = self.timer

    def usedTime(self,txt):
        end = time.time()
        step_time = end - self.last_time
        all_time = end - self.timer

        print("%s: %s %s" % (txt,step_time,all_time))
        self.last_time = time.time()


timer = Timer()

print("load ML")
ml = multilayer.MultiLayerGraph.load("data/multilayer_co_author_influence_collaboration.pickle")
timer.usedTime("ML")
gr  =ml.ynws[1980].multi_layer
gr.vs["weight"] = 1
gr.es["weight"] = 1
weights_v = gr.vs["weight"]

timer.usedTime("before groups")
#df_g_s,df_g_t,df = FormanRicci.FormanRicci.edgeWeigthsToGroups(gr)

FR = FormanRicci.FormanRicci.FormanRicchi()
FR.init_F2(gr,use_dict= False, use_dict2= False)
timer.usedTime("before FM1")
#pr.enable()
x = FR.F_df(1)
#pr.disable()
timer.usedTime("no dict")

FR.init_F2(gr,use_dict=True, use_dict2= False)
FR.F_df.cache_clear()
timer.usedTime("init")
#pr.enable()
x = FR.F_df(1)
#pr.disable()
timer.usedTime("dict")

FR.init_F2(gr,use_dict=True,use_dict2=True)
FR.F_df.cache_clear()
timer.usedTime("init")
#pr.enable()
x = FR.F_df(1)
#pr.disable()
timer.usedTime("dict 2")

x1 = FR.F(gr.es[1])
timer.usedTime("other impl")
assert x1 == x, "should be the same"


FR.init_F2(gr,use_dict=True,use_dict2=True)
FR.F_df.cache_clear()
FR.F_v_df.cache_clear()
timer.usedTime("init")
y = FR.F_v_df(3)
timer.usedTime("F_v data frame dict2")
FR.init_F2(gr,use_dict=True,use_dict2=True)
FR.F_df.cache_clear()
FR.F_v_df.cache_clear()
timer.usedTime("init")
y = FR.F_v_df(3)
timer.usedTime("F_v data frame dict")

FR.init_F2(gr,use_dict=False,use_dict2=False)
FR.F_df.cache_clear()
FR.F_v_df.cache_clear()
timer.usedTime("init")
y = FR.F_v_df(3)
timer.usedTime("F_v data frame no dict ")

FR.F.cache_clear()
FR.F_v_df.cache_clear()
y1 = FR.F_v(3)
timer.usedTime("other impl F_V")
assert y1 == y, "should %s be the same as %s" % (y1,y)

FR.F.cache_clear()
FR.F_v_df.cache_clear()
timer.usedTime("init")
z = FR.forman_edges_df(pool=10)
timer.usedTime("dataframe impl forman edges")

FR.F.cache_clear()
FR.F_v_df.cache_clear()
timer.usedTime("init")
z1 = FR.forman_edges(pool=10)
timer.usedTime("other impl forman edges")


assert z == z1, "should %s be the same as %s" % (z1,z)

FR.F.cache_clear()
FR.F_v_df.cache_clear()
timer.usedTime("init")
z = FR.forman_vertices_df(pool=10)
timer.usedTime("dataframe impl forman vertices")

FR.F.cache_clear()
FR.F_v_df.cache_clear()
timer.usedTime("init")
z1 = FR.forman_vertices(pool=10)
timer.usedTime("other impl forman vertices")


assert z == z1, "should %s be the same as %s" % (z1,z)



s = io.StringIO()
sortby = SortKey.CUMULATIVE
#ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
#ps.print_stats()
#print(s.getvalue())

