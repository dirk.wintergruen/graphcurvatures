from distutils.core import setup
from Cython.Build import cythonize
from distutils.extension import Extension
#from setuptools import setup

setup(
    name='Curvatures',
    version='0.1.2',
    packages=['FormanRicci'],
    url='',
    license='',
    author='dwinter',
    author_email='',
    ext_modules = cythonize("FormanRicci/*.pyx"),
    description=''
)
