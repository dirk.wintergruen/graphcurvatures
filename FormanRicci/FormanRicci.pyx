import sys
from collections import defaultdict

import igraph
import numpy
import pandas
import os.path
from functools import lru_cache
from math import sqrt
from tqdm import tqdm_notebook,tqdm
from multiprocessing import Pool
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class FormanRicci(object):
    @lru_cache(maxsize=None)
    def F(self,e, directed=False):
        nw = self.nw

        if isinstance(e,int):
           e = nw.es[e]
        v1 = e.source
        v2 = e.target

        if directed:
            e_v1 = [x for x in nw.es.select(_source=v1) if x != e]
            e_v2 = [x for x in nw.es.select(_source=v2) if x != e]

        else:
            e_v1 = [x for x in nw.es.select(_source=v1) if x != e] + [x for x in nw.es.select(_target=v1) if x != e]
            e_v2 = [x for x in nw.es.select(_source=v2) if x != e] + [x for x in nw.es.select(_target=v2) if x != e]
        edge_term = 0

        for e1 in e_v2:
            edge_term += nw.vs["weight"][v2] / sqrt(e["weight"] * e1["weight"])


        for e2 in e_v1:
            edge_term += nw.vs["weight"][v1] / sqrt(e["weight"] * e2["weight"])

        weight_term = nw.vs["weight"][v1] / e["weight"] + nw.vs["weight"][v2] / e["weight"]

        # print(edge_term,weight_term)
        f = e["weight"] * (weight_term - edge_term)

        return f


    @lru_cache(maxsize=None)
    def F_v(self,v, directed=False):
        nw = self.nw
        if not isinstance(v, int):
            v = v.index

        deg = nw.degree(v)

        if deg == 0:
            return numpy.Infinity

        if directed:
            e_v = [x for x in nw.es.select(_source=v)]
        else:
            e_v = [x for x in nw.es.select(_source=v)] + [x for x in nw.es.select(_target=v)]

        f = 0
        for e in e_v:
            f += self.F(e,directed=directed)

        f = 1 / nw.degree(v) * f

        return f


    # def calc_curv(self,ynws):
    #     y_f = {}
    #     for y, gr in tqdm(ynws.items()):
    #         curv = []
    #         g = gr.multi_layer.simplify(multiple=False)
    #         # print(len(g.vs),len(g.es))
    #         g.vs["weight"] = 1
    #         g.es["weight"] = 1
    #         # for v in g.vs:
    #         #    curv.append(F_v(v,g))
    #         p_g = [(p.index, g) for p in g.vs]
    #         with Pool(10) as p:
    #             curv = p.map(F_v, p_g)
    #         y_f[y] = curv
    #     return y_f


    # def calc_curv_edge(self,ynws):
    #     y_f = {}
    #     for y, gr in tqdm(ynws.items()):
    #         curv = []
    #         g = gr.multi_layer.simplify(multiple=False)
    #         # print(len(g.vs),len(g.es))
    #         g.vs["weight"] = 1
    #         g.es["weight"] = 1
    #         p_g = [(p.index, g) for p in g.es]
    #         with Pool(20) as p:
    #             curv = p.map(self.F, p_g)
    #         y_f[y] = curv
    #     return y_f


    def forman_edges(self, pool=20,debug = False):
        if debug:
            logging.info("debug")
            ps = range(0,10)
        else:
            ps = range(0,len(self.nw.es))

        if pool == 1:
            return [self.F(p) for p in ps]

        with Pool(pool) as p:
            curv = p.map(self.F, ps)
        return curv


    def forman_vertices(self,pool=20,debug = False):
        if debug:
            logging.info("debug")
            ps = range(0,10)
        else:
            ps = range(0,len(self.nw.vs))

        if pool == 1:
            return [self.F_v(p) for p in ps]

        with Pool(pool) as p:
            curv = p.map(self.F_v, ps)
        return curv

    def edgeWeigthsToGroups(self):
        el = self.nw.get_edgelist()
        df = pandas.DataFrame(el,columns=["s","t"])
        df["weight"] = self.nw.es["weight"]

       # df_g_s = df.groupby(by=["s"])
       # df_g_t = df.groupby(by=["t"])

        df_g_s = defaultdict(list)
        df_g_t = defaultdict(list)
        cnt = 0
        for s,t in zip(df["s"].values,df["t"]):
            df_g_s[s].append(cnt)
            df_g_t[t].append(cnt)
            cnt+=1
        return df_g_s,df_g_t,df ##dict to make them hashable


    #@lru_cache(maxsize=None)
    def getEdges(self,v,directed=False):

        edges = list(self.df_g_s[v])

        if not directed:
            edges += list(self.df_g_t[v])

        return edges

    #@lru_cache(maxsize=None)
    def getWeights(self,e,edges_s_weight,edges_t_weight):

       #get the subframe without the e
       e_s = edges_s_weight #.drop(e,axis=0)
       e_t = edges_t_weight #.drop(e,axis=0)

       return e_s["weight"].values,e_t["weight"].values



    @lru_cache(maxsize=None)
    def F_df(self,e,directed=False):

        try:
            s = self.df2["s"][e]
            t = self.df2["t"][e]
            e_weight = self.df2["weight"][e]
        except:
            s = self.df["s"][e]
            t = self.df["t"][e]
            e_weight = self.df["weight"][e]

        edge_term = 0

        edges_s = self.getEdges(s,directed=directed)
        edges_t = self.getEdges(t,directed=directed)


        
        edges_s.remove(e) # remove the edge itself
        edges_t.remove(e) # remove the edge itself

        edges_s_weight = self.df.loc[edges_s]
        edges_t_weight = self.df.loc[edges_t]



        #edges_s_weight = df.loc[df_g_s.groups[s]] # get  the subframe with edges starting with s
        #edges_t_weight = df.loc[df_g_t.groups[s]] # get  the subframe with edges starting with t

        #now we get the weights to these edges
        w_s,w_t = self.getWeights(e,edges_s_weight,edges_t_weight)
        #w_t, w_s = self.getWeights(e, edges_s_weight, edges_t_weight)

        for w in w_s:
            edge_term += self.weights_v[s] / sqrt(e_weight * w)


        for w in w_t:
            edge_term += self.weights_v[t] / sqrt(e_weight * w)

        weight_term = self.weights_v[s] / e_weight + self.weights_v[t] / e_weight

        # print(edge_term,weight_term)
        f = e_weight * (weight_term - edge_term)

        return f

    @lru_cache(maxsize=None)
    def F_v_df(self, v, directed=False):

        deg = self.degree[v]
        if deg == 0:
            return numpy.Infinity

        e_v = self.getEdges(v, directed=directed)


        f = 0
        for e in e_v:
            f += self.F_df(e, directed=directed)

        f = 1 / deg * f

        return f

    def init_F2(self,nw,use_dict=True,use_dict2=True): ##init for method with dataframes instead of nw
        self.nw = nw
        self.degree = {n.index: nw.degree(n.index) for n in nw.vs}

        assert "weight" in nw.vertex_attributes(), "nw vertices have no attribute weight"
        assert "weight" in nw.edge_attributes(), "nw edges have no attribute weight"
        if use_dict:
            self.weights_v = dict(enumerate(nw.vs["weight"]))
        else:
            self.weights_v = nw.vs["weight"]



        self.df_g_s, self.df_g_t, self.df = self.edgeWeigthsToGroups()
        if use_dict2:
            df2 = {}
            df2["s"] = dict(enumerate(self.df["s"].values))
            df2["t"] = dict(enumerate(self.df["t"].values))
            df2["weight"] = dict(enumerate(self.df["weight"].values))
            self.df2 = df2

    def forman_edges_df(self, pool=20, debug=False):
        if debug:
            logging.info("debug")
            ps =  range(0,10)
        else:
            ps =  range(0,len(self.nw.es))

        if pool == 1:
            return [self.F_df(p) for p in ps]

        with Pool(pool) as p:
            curv = p.map(self.F_df, ps)
        return curv

    def forman_vertices_df(self, pool=20, debug=False):
        if debug:
            logging.info("debug")
            ps = range(0, 10)
        else:
            ps = range(0, len(self.nw.vs))

        if pool == 1:
            return [self.F_v_df(p) for p in ps]

        with Pool(pool) as p:
            curv = p.map(self.F_v_df, ps)
        return curv

    def main(self,nw_path,out_path,edges=False,vertices=False,
             use_dict=False,use_dict2=False,df_mode=False,pool=20,
             debug=False):
        nw = igraph.load(nw_path)

        self.init_F2(nw,use_dict=use_dict,use_dict2=use_dict2)

        if edges:

            curv_edges = self.forman_edges(pool=pool,debug=debug)
            with open(os.path.join(out_path,"edges.csv"),"w") as outf:
                for x in curv_edges:
                    outf.write("%s\n" % x)
            print("wrote edges to: %s" % os.path.join(out_path,"edges.csv"))
        if vertices:

            curv_vertices = self.forman_vertices(pool=pool,debug=debug)
            with open(os.path.join(out_path,"edges.csv","w")) as outf:
                for x in curv_vertices:
                    outf.write("%s\n" % x)
            print("wrote vertices to: %s" % os.path.join(out_path,"vertices.csv"))






