"""Follow paper XX
We create an influence network
"""

import igraph
import numpy


def gamma(gr):
    """Construct edges of influence network eq. 2/3"""
    gamma = gr.shortest_paths_dijkstra(mode=igraph.ALL)
    # generate a numpy matrix

    gamma = numpy.array([numpy.array(x) for x in gamma])

    # generate weights depending on length of shortest path
    # g(e) = 1/deg(e) * 1 /l if l <= 6, 0 otherwise

    gamma[gamma > 6] = numpy.Infinity
    gamma[gamma == 0] = numpy.Infinity
    gamma = 1 / gamma


    return gamma


def omega(gr,gamma):
    """construct weight on nodes from edge weights
    omega(v) =  1 /deg(v) * sum g(e) with e adjacent to v
    """

    # degree ist the number of non zero entries per row

    deg_mat = gamma.copy()
    deg_mat[deg_mat > 0] = 1
    degree = deg_mat.sum(axis=0)

    omega = gamma.sum(axis=0) #sum all weights connected to an vertex (sum of a row / column assuming symmetry)

    omega = 1/degree * omega

    return omega