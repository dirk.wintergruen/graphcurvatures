import sys
import argparse
import os

from FormanRicci.FormanRicci import FormanRicci

parser = argparse.ArgumentParser()
parser.add_argument("nw_path", help="path to network file")
parser.add_argument("out_path", help="path where to write the results to")
parser.add_argument("-e", "--edges", default=False, const=True, nargs="?", help="calculate curvature for edges")
parser.add_argument("-v", "--vertices", default=False, const=True, nargs="?", help="calculate curvature for vertices")
parser.add_argument("-d", "--use_dict", default=False, const=True, nargs="?", help="use dict")
parser.add_argument("-d2", "--use_dict2", default=False, const=True, nargs="?", help="use dict2")
parser.add_argument("--debug", default=False, const=True, nargs="?", help="use dict2")

args = parser.parse_args()
if (not args.edges) & (not args.vertices):
    print("neither curvature for edges nor for vertices is to be calculated. I will stop here")
    sys.exit(-1)

os.makedirs(args.out_path, exist_ok=True)
fr = FormanRicci()

fr.main(args.nw_path,
        args.out_path,
        edges=args.edges,
        vertices=args.vertices,
        use_dict=args.use_dict,
        use_dict2=args.use_dict2,
        debug=args.debug,
        df_mode=True)